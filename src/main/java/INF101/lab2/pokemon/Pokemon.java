package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    private String name;
    private int healthPoints;
    private int maxHealthPoints;
    private int strength;
    private Random random;

    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }


    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        return getCurrentHP() > 0;
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(name + " attacks " + target.getName() + ".");
        target.damage(damageInflicted);
        if (!target.isAlive()) {
            System.out.println(target.getName() + " is defeated by " + name + ".");
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken < 0) {
            return;
        }

        if ((healthPoints - damageTaken) < 0) {
            healthPoints = 0;
        } else {
            healthPoints -= damageTaken;
        }

        System.out.println(
                name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" +
                        maxHealthPoints + " HP");
    }

    @Override
    public String toString() {
        return name + " HP: (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }
}
